<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "Cool blooded : " . $sheep->cool_blooded . "<br> <br>";

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . "<br>";
echo "Legs : " . $sungokong->legs . "<br>";
echo "Cool blooded : " . $sungokong->cool_blooded . "<br>";
echo "Yell : " . $sungokong->yell() . "<br><br>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>";
echo "Legs : " . $kodok->legs . "<br>";
echo "Cool blooded : " . $kodok->cool_blooded . "<br>";
echo "Jump : " . $kodok->jump();

?>