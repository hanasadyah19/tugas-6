<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<form action="/kirim" method="POST">
    @csrf
    <h1>Buat Akun Baru</h1>
    <h2>Sign Up Form</h2>
    <label for="">First Name:</label><br><br>
    <input type="text" name="firstname"><br><br>
        <label for="">Last Name:</label><br><br>
        <input type="text" name="lastname"><br><br>
        <label> Gender: </label><br><br>
        <input type="radio" name="Geder" value="1" >Male <br><br>
        <input type="radio" name="Geder" value="2" >Female <br><br>
        <input type="radio" name="Geder" value="3" >Other <br><br>
        <label>Nationality:</label> <br><br>
        <select name="Nationality">
            <option value="Indonesian" >Indonesian</option>
            <option value="Malaysian" >Malaysian</option>
            <option value="Australian" >Australian</option>
            <option value="American" >American</option>
            <option value="British" >British</option>
            <option value="Other" >Other</option></select><br><br>
        <label> Language Spoken: </label><br><br>
        <input type="checkbox"> Bahasa Indonesia<br>
        <input type="checkbox"> English<br>
        <input type="checkbox"> Other <br><br>
        <label> Bio: </label><br><br>
        <textarea name="Bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
</form>
</body>
</html>