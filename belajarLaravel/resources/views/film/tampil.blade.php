@extends('layout.master')

@section('title')
    Halaman Table Film
@endsection

@section('content')

<a href="/film/create" class="btn btn-primary btn-sm my-3">Tambah Film</a>

<div class="row">

    @forelse ($film as $item)
    <div class="col-4">
        <div class="card">
            <img src="{{asset('/image/'. $item->poster)}}" class="card-img-top" style="height: 300px" alt="...">
                <div class="card-body">
                    <h3>{{$item->judul}}</h3>
                    <p class="card-text">{{Str::limit($item->ringkasan, 40)}}</p>
                    <a href="/film/{{$item->id}}" class="btn btn-primary btn-block">Watch More</a>
                    <div class="row my-2">
                        <div class="col">
                            <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-block">Edit</a>
                        </div>
                        <div class="col">
                            <form action="/film/{{$item->id}}" method="POST">
                                @method('delete')
                                @csrf
                                <input type="submit" class="btn btn-danger btn-block" value="Delete">
                            </form>   
                        </div>
                    </div>
                </div>
        </div>
    </div>
    
</div>

@empty
    <h1>Film masih kosong</h1>
@endforelse


@endsection