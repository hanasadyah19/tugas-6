@extends('layout.master')

@section('title')
    Halaman Edit Genre
@endsection

@section('content')

<form action="/genre/{{$genre->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label >Nama Genre</label>
      <input type="text" value="{{$genre->nama}}" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection