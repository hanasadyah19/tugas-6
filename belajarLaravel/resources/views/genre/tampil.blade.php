@extends('layout.master')

@section('title')
    Halaman Table Genre
@endsection

@section('content')

<a href="/genre/create" class="btn btn-primary btn-sm my-3">create</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($data as $key => $item)
            <tr>
                 <td>{{$key + 1}}</td>
                 <td>{{$item->nama}}</td>
                 <td>
                    <form action="/genre/{{$item->id}}" method="POST">
                    <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @method('delete')
                    @csrf
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
                 </td>
            </tr>
        @empty
        <h1>Data Genre Kosong</h1>
        @endforelse
        
    </tbody>

@endsection