<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\FilmController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);

Route::get('/signup', [AuthController::class, 'register']);

Route::post('/kirim', [AuthController::class, 'welcome']);

Route::get('/table', function(){
    return view('page.table');
});

Route::get('/data-table', function(){
    return view('page.data-table');
});

//create data
Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);

//read data
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//edit data
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

//update data
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//delete data
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);

//CRUD genre
Route::get('/genre/create', [GenreController::class, 'create']);
Route::post('/genre', [GenreController::class, 'store']);
Route::get('/genre', [GenreController::class, 'index']);
Route::get('/genre/{genre_id}', [GenreController::class, 'show']);
Route::get('/genre/{genre_id}/edit', [GenreController::class, 'edit']);
Route::put('/genre/{genre_id}', [GenreController::class, 'update']);
Route::delete('/genre/{genre_id}', [GenreController::class, 'destroy']);

//CRUD Film
Route::resource('film', FilmController::class);