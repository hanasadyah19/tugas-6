<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GenreController extends Controller
{
    public function create()
    {
        return view('genre.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required'
        ],
        [
            'nama.required' => 'Nama genre harus diisi',
        ]);
        DB::table('genre')->insert([
            'nama' => $request['nama']
        ]);

        return redirect('/genre');
    }

    public function index()
    {
        $data = DB::table('genre')->get();
        //dd($data);

        return view('genre.tampil', ['data' => $data]);
    }

    public function show($id)
    {
        $genre = DB::table('genre')->find($id);

        return view('genre.detail', ['genre' => $genre]);
    }

    public function edit($id)
    {
        $genre = DB::table('genre')->find($id);

        return view('genre.edit', ['genre' => $genre]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required'
        ],
        [
            'nama.required' => 'Nama genre harus diisi',
        ]);
        DB::table('genre')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request['nama'],
                ]);
        return redirect('/genre');
    }

    public function destroy($id)
    {
        DB::table('genre')->where('id', '=', $id)->delete();

        return redirect('/genre');
    }
}
