<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('signup');
    }

    public function welcome(Request $request)
    {
       // dd($request->all());
       $namaDepan = $request['firstname'];
       $namaBelakang = $request['lastname'];

       return view('home', ['firstname' => $namaDepan, 'lastname' => $namaBelakang]);
    }
}
