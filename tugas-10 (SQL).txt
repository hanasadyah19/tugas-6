1. Buat Database
CREATE database myshop; 

2. Buat Tabel

Users
CREATE TABLE users( id int(8) AUTO_INCREMENT PRIMARY KEY, name varchar(225) NOT null, email varchar(225) NOT null, password varchar(225) NOT null );

Categories
CREATE TABLE categories( id int(8) AUTO_INCREMENT PRIMARY KEY, name varchar(225) NOT null ); 

Items
CREATE TABLE items( id int(8) AUTO_INCREMENT PRIMARY KEY, name varchar(225) NOT null, description varchar(225) NOT null, price int(8) NOT null, stock int(8) NOT null, category_id int(8) NOT null, FOREIGN key(category_id) REFERENCES categories(id) ); 

3. Insert Data

User
INSERT INTO users(name, email, password) VALUES("John Doe", "john@doe.com", "john123"); 
INSERT INTO users(name, email, password) VALUES("Jane Doe", "jane@doe.com", "jenita123")

Categories
INSERT INTO categories(name) VALUES("gadget"); 
INSERT INTO categories(name) VALUES("cloth"); 
INSERT INTO categories(name) VALUES("men"); 
INSERT INTO categories(name) VALUES("women"); 
INSERT INTO categories(name) VALUES("branded"); 

Items 
INSERT INTO items(name, description, price, stock, category_id) VALUES("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1); 
INSERT INTO items(name, description, price, stock, category_id) VALUES("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2); 
INSERT INTO items(name, description, price, stock, category_id) VALUES("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1); 

4. Select Data

a.
SELECT id, name, email FROM users; 

b.
- SELECT * FROM items WHERE price > 1000000; 
- SELECT * FROM items WHERE name LIKE "%sang%"; 

c.
SELECT items.* , categories.name AS kategori FROM items INNER JOIN categories ON items.category_id = category_id; 

5. Update Data

UPDATE `items` SET `price`=2500000 WHERE 1; 
